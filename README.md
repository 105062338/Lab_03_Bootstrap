# Software Studio 2018 Spring Lab03 Bootstrap and RWD
![Bootstrap logo](./Bootstrap logo.jpg)
## Grading Policy
* **Deadline: 2018/03/20 17:20 (commit time)**

## Todo
1. Check your username is Student ID
2. **Fork this repo to your account, remove fork relationship and change project visibility to public**
3. Make a personal webpage that has RWD
4. Use bootstrap from [CDN](https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css)
5. Use any template form the inernet but you should provide the URL in your project README.md
6. Use at least **Ten** bootstrap elements by yourself which is not include the template
7. You web page template should totally fit to your original personal web page content
8. Modify your markdown properly (check all todo and fill all item)
9. Deploy to your pages (https://[username].gitlab.io/Lab_03_Bootstrap)
10. Go home!

## Student ID , Name and Template URL
- [ ] Student ID : 105062338
- [ ] Name :周廷駿
- [ ] URL :https://getbootstrap.com/docs/4.0/examples/cover/

## 10 Bootstrap elements (list in here)
1. images ---headshot
2. helpers---哈囉你好嗎
3. button ---don't click
4. button ---music
5. button ---click warning
6. images ---music
7. Typography --- Page
8. table ---hobbyyy
9. form
10. text ---感謝有你<3
